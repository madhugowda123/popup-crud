import { Form, Button } from "react-bootstrap";

import { EmployeeContext } from "../contexts/TableContext";
import { useContext, useState } from "react";

const AddForm = () => {
  const { addEmployee } = useContext(EmployeeContext);

  const [newEmployee, setNewEmployee] = useState({
    education: "",
    institute: "",
    university: "",
    passingyear: "",
    percentage: "",
  });

  const onInputChange = (e) => {
    setNewEmployee({ ...newEmployee, [e.target.name]: e.target.value });
  };

  const {
    education,
    institute,
    university,
    passingyear,
    percentage,
  } = newEmployee;

  const handleSubmit = (e) => {
    e.preventDefault();
    addEmployee(education, institute, university, passingyear, percentage);
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Education"
          name="education"
          value={education}
          onChange={(e) => onInputChange(e)}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Institute "
          name="institute"
          value={institute}
          onChange={(e) => onInputChange(e)}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="University"
          name="university"
          value={university}
          onChange={(e) => onInputChange(e)}
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Passingyear"
          name="passingyear"
          value={passingyear}
          onChange={(e) => onInputChange(e)}
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Percentage"
          name="percentage"
          value={percentage}
          onChange={(e) => onInputChange(e)}
        />
      </Form.Group>
      <Button variant="primary" type="submit" block>
        Add New
      </Button>
    </Form>
  );
};

export default AddForm;
