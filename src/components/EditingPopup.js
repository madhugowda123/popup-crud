import { Form, Button } from "react-bootstrap";

import { EmployeeContext } from "../contexts/TableContext";
import { useContext, useState } from "react";

const EditForm = ({ theEmployee }) => {
  const id = theEmployee.id;

  const [education, setEducation] = useState(theEmployee.education);
  const [institute, setInstitute] = useState(theEmployee.institute);
  const [university, setUniversity] = useState(theEmployee.university);
  const [passingyear, setPassingyear] = useState(theEmployee.passingyear);
  const [percentage, setPercentage] = useState(theEmployee.percentage);

  const { updateEmployee } = useContext(EmployeeContext);

  const updatedEmployee = {
    education,
    institute,
    university,
    passingyear,
    percentage,
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    updateEmployee(id, updatedEmployee);
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Education *"
          name="education"
          value={education}
          onChange={(e) => setEducation(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Institute *"
          name="institute"
          value={institute}
          onChange={(e) => setInstitute(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="University"
          name="university"
          value={university}
          onChange={(e) => setUniversity(e.target.value)}
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Passing Year"
          name="passingyear"
          value={passingyear}
          onChange={(e) => setPassingyear(e.target.value)}
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Percentage"
          name="percentage"
          value={percentage}
          onChange={(e) => setPercentage(e.target.value)}
        />
      </Form.Group>
      <Button variant="success" type="submit" block>
        Update
      </Button>
    </Form>
  );
};

export default EditForm;
