import { useContext, useState, useEffect } from "react";
import React from "react";
import { EmployeeContext } from "../contexts/TableContext";
import { Modal, Button } from "react-bootstrap";
import EditForm from "./EditingPopup";

const Employee = ({ employee }) => {
  const { deleteEmployee } = useContext(EmployeeContext);

  const [show, setShow] = useState(false);

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  useEffect(() => {
    handleClose();
  }, [employee]);

  return (
    <React.Fragment>
      <td>{employee.education}</td>
      <td>{employee.institute}</td>
      <td>{employee.university}</td>
      <td>{employee.passingyear}</td>
      <td>{employee.percentage}</td>

      <td>
        <button
          onClick={handleShow}
          className="btn text-warning btn-act"
          data-toggle="modal"
        >
          Edit
        </button>
        <button
          onClick={() => deleteEmployee(employee.id)}
          className="btn text-danger btn-act"
          data-toggle="modal"
        >
          Delete
        </button>
      </td>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <EditForm theEmployee={employee} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  );
};

export default Employee;
