import { Modal, Button } from "react-bootstrap";
import React from "react";
import { useContext, useEffect, useState } from "react";
import { EmployeeContext } from "../contexts/TableContext";
import Employee from "./TableDetails";
import AddForm from "./AddingPopup";
import Pagination from "./Pagination";

const EmployeeList = () => {
  const { sortedEmployees } = useContext(EmployeeContext);

  const [show, setShow] = useState(false);

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const [currentPage, setCurrentPage] = useState(1);
  const [employeesPerPage] = useState(8);

  useEffect(() => {
    handleClose();

    return () => {
      // handleShowAlert();
    };
  }, [sortedEmployees]);

  const indexOfLastEmployee = currentPage * employeesPerPage;
  const indexOfFirstEmployee = indexOfLastEmployee - employeesPerPage;
  const currentEmployees = sortedEmployees.slice(
    indexOfFirstEmployee,
    indexOfLastEmployee
  );
  const totalPagesNum = Math.ceil(sortedEmployees.length / employeesPerPage);

  return (
    <React.Fragment>
      <table className="table  table-hover">
        <thead>
          <tr>
            <th>Education</th>
            <th>Institute</th>
            <th>University</th>
            <th>Passing Year</th>
            <th>Percentage</th>

            <th>
              {" "}
              <Button
                onClick={handleShow}
                className="btn btn-primary "
                data-toggle="modal"
              >
                <span>Add New</span>
              </Button>
            </th>
          </tr>
        </thead>
        <tbody>
          {currentEmployees.map((employee) => (
            <tr key={employee.id}>
              <Employee employee={employee} />
            </tr>
          ))}
        </tbody>
      </table>

      <Pagination
        pages={totalPagesNum}
        setCurrentPage={setCurrentPage}
        currentEmployees={currentEmployees}
        sortedEmployees={sortedEmployees}
      />

      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>Add</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddForm />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  );
};

export default EmployeeList;
