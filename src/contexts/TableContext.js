import { createContext, useEffect, useState } from "react";
import { v4 as uuidv4 } from "uuid";

export const EmployeeContext = createContext();

const EmployeeContextProvider = (props) => {
  const [employees, setEmployees] = useState([]);
  const sortedEmployees = employees.sort((a, b) => (a.name < b.name ? -1 : 1));
  const addEmployee = (
    education,
    institute,
    university,
    passingyear,
    percentage
  ) => {
    setEmployees([
      ...employees,
      {
        id: uuidv4(),
        education,
        institute,
        university,
        passingyear,
        percentage,
      },
    ]);
  };

  const deleteEmployee = (id) => {
    setEmployees(employees.filter((employee) => employee.id !== id));
  };

  const updateEmployee = (id, updatedEmployee) => {
    setEmployees(
      employees.map((employee) =>
        employee.id === id ? updatedEmployee : employee
      )
    );
  };

  return (
    <EmployeeContext.Provider
      value={{ sortedEmployees, addEmployee, deleteEmployee, updateEmployee }}
    >
      {props.children}
    </EmployeeContext.Provider>
  );
};

export default EmployeeContextProvider;
