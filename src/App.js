import EmployeeList from "./components/Table";
import EmployeeContextProvider from "./contexts/TableContext";
function App() {
  return (
    <div className="container-xl">
      <div className="table-responsive">
        <div className="table-wrapper">
          <EmployeeContextProvider>
            <EmployeeList />
          </EmployeeContextProvider>
        </div>
      </div>
    </div>
  );
}
export default App;
